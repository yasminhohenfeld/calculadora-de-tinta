# Calculadora de Tinta

## Visão Geral

Bem-vindo(a) ao projeto da Calculadora de Tinta. Este projeto consiste em uma aplicação backend que calcula a quantidade de tinta necessária para pintar uma sala baseada nas dimensões das paredes, quantidade de janelas e portas.

Para rodar a aplicação você precisará abrir dois terminais no VsCode rodar o backend e rodar o frontend, você poderá validar tanto no backend como direto no frontend na rota: http://localhost:3000/ pelo frontend e backend: localhost:3001/api/calculate

## 💻 Backend

### Pré-requisitos

- É necessário ter o Node.js instalado na sua máquina. Você pode baixá-lo em [nodejs.org](https://nodejs.org/).

### Instalação das Dependências

1. Clone este repositório para sua máquina local;
2. Abra o terminal e navegue até a pasta do projeto backend. Em seguida, execute o seguinte comando para instalar as dependências:

```
npm install
```

### Rodar a Aplicação

1. Após a instalação das dependências, execute o seguinte comando para iniciar a aplicação:

```
node index.js
```


2. Se tudo estiver configurado corretamente, você verá a mensagem no terminal: 'Servidor rodando na porta 3000'.

### Informações


- Eu utilizei o Insomnia para testar a aplicação.
- No corpo da requisição, envie um objeto no seguinte formato:

```json
{
   "paredes": [
       {
           "largura": 5,
           "altura": 5,
           "qtdJanelas": 1,
           "qtdPortas": 1
       }
   ]
}
```

- Se tudo estiver correto, você receberá uma resposta com a mensagem: 'Cálculo de tinta realizado com sucesso.


### Tecnologias Utilizadas no Backend

- Node.js
- Express
- Yup (validação de dados)

## 💻 Frontend

### Pré-requisitos

- É necessário ter o Node.js instalado na sua máquina. 

#### Estrutura do Projeto

```
calculadora-de-tinta/
├── public/
│   ├── index.html
│   ├── manifest.json
│   └── robots.txt
├── src/
│   ├── components/
│   │   ├── PaintCalculator.js
│   │   ├── PaintResults.js
│   │   └── WallForm.js
│   ├── App.css
│   ├── App.js
│   ├── App.test.js
│   ├── index.css
│   ├── index.js
│   └── reportWebVitals.js
├── .gitignore
├── package.json
└── README.md
```
### Instalação das dependências e rodar aplicação

1 - Abra o terminal e navegue até a pasta do projeto frontend e execute:

```
cd calculadora-de-tinta
```

2 - Execute o seguinte comando para instalar as dependências:

```
npm install
```

3 - Para rodar, após as instalações execute para iniciar:

```
npm start
```

### Informações

1 - A aplicação será rodada no navegador em http://localhost:3000 

### Tecnologias Utilizadas no Frontend

- React
- Javascript
- HTML
- CSS


