import React from 'react';

function WallForm({ index, wall, onWallChange }) {
  return (
    <div>
      <h2>Parede {index + 1}</h2>
      <label>
        Largura (m):
        <input
          type="number"
          value={wall.width}
          onChange={(e) => onWallChange(index, 'width', e.target.value)}
          min="1"
          max="50"
          required
        />
      </label>
      <label>
        Altura (m):
        <input
          type="number"
          value={wall.height}
          onChange={(e) => onWallChange(index, 'height', e.target.value)}
          min="1"
          max="50"
          required
        />
      </label>
      <label>
        Janelas:
        <input
          type="number"
          value={wall.windows}
          onChange={(e) => onWallChange(index, 'windows', e.target.value)}
          min="0"
          required
        />
      </label>
      <label>
        Portas:
        <input
          type="number"
          value={wall.doors}
          onChange={(e) => onWallChange(index, 'doors', e.target.value)}
          min="0"
          required
        />
      </label>
    </div>
  );
}

export default WallForm;
