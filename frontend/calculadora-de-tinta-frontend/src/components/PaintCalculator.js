import React, { useState } from 'react';
import axios from 'axios';
import WallForm from './WallForm';
import PaintResults from './PaintResults';

function PaintCalculator() {
  const [walls, setWalls] = useState([{ width: '', height: '', windows: '', doors: '' }]);
  const [results, setResults] = useState(null);

  const handleWallChange = (index, field, value) => {
    const newWalls = [...walls];
    newWalls[index][field] = value;
    setWalls(newWalls);
  };

  const addWall = () => {
    setWalls([...walls, { width: '', height: '', windows: '', doors: '' }]);
  };

  const calculatePaint = async () => {
    try {
      const response = await axios.post('http://localhost:3000/calcular', { paredes: walls });
      setResults(response.data);
    } catch (error) {
      console.error('Erro ao calcular a tinta:', error.response.data.message);
    }
  };

  return (
    <div>
      <h1>Calculadora de Tinta</h1>
      {walls.map((wall, index) => (
        <WallForm
          key={index}
          index={index}
          wall={wall}
          onWallChange={handleWallChange}
        />
      ))}
      <button onClick={addWall}>Adicionar Parede</button>
      <button onClick={calculatePaint}>Calcular Tinta</button>
      {results && <PaintResults results={results} />}
    </div>
  );
}

export default PaintCalculator;
