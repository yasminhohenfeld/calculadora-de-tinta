import React from 'react';

function PaintResults({ results }) {
  return (
    <div>
      <h2>Resultados</h2>
      <p>Litros necessários: {results.litrosNecessarios}</p>
      <ul>
        {results.latas.map((lata, index) => (
          <li key={index}>
            {lata.tamanho}L: {lata.quantidade} lata(s)
          </li>
        ))}
      </ul>
    </div>
  );
}

export default PaintResults;
