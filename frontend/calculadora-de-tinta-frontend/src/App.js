import React, { useState } from 'react';
import axios from 'axios';
import './App.css';

function App() {
  const [walls, setWalls] = useState([{ largura: '', altura: '', qtdJanelas: '', qtdPortas: '' }]);
  const [response, setResponse] = useState(null);
  const [error, setError] = useState(null);

  const handleChange = (index, event) => {
    const values = [...walls];
    values[index][event.target.name] = event.target.value;
    setWalls(values);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    axios.post('http://localhost:3001/api/calculate', { paredes: walls })
      .then(res => {
        setResponse(res.data);
        setError(null);
      })
      .catch(err => {
        setResponse(null);
        setError(err.response.data.message);
      });
  };

  const addWall = () => {
    setWalls([...walls, { largura: '', altura: '', qtdJanelas: '', qtdPortas: '' }]);
  };

  return (
    <div>
      <form onSubmit={handleSubmit}>
        {walls.map((wall, index) => (
          <div key={index}>
            <input
              type="number"
              name="largura"
              value={wall.largura}
              onChange={(event) => handleChange(index, event)}
              placeholder="Largura"
            />
            <input
              type="number"
              name="altura"
              value={wall.altura}
              onChange={(event) => handleChange(index, event)}
              placeholder="Altura"
            />
            <input
              type="number"
              name="qtdJanelas"
              value={wall.qtdJanelas}
              onChange={(event) => handleChange(index, event)}
              placeholder="Janelas"
            />
            <input
              type="number"
              name="qtdPortas"
              value={wall.qtdPortas}
              onChange={(event) => handleChange(index, event)}
              placeholder="Portas"
            />
          </div>
        ))}
        <button type="button" onClick={addWall}>Adicionar Parede</button>
        <button type="submit">Calcular Tinta</button>
      </form>

      {error && <p>{error}</p>}

      {response && (
        <div>
          <p>Área Total: {response.totalArea} m²</p>
          <p>Área de Aberturas: {response.totalOpenings} m²</p>
          <p>Área Pintável: {response.paintableArea} m²</p>
          <p>Tinta Necessária: {response.paintRequired} litros</p>
          <p>Latas de Tinta:</p>
          <ul>
            {response.cans.map((can, index) => (
              <li key={index}>{can.quantidade} latas de {can.tamanho} litros</li>
            ))}
          </ul>
        </div>
      )}
    </div>
  );
}

export default App;
