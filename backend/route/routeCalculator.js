const express = require('express');
const router = express.Router();
const { calculatePaint } = require('../control/calculatorControl');

router.post('/calculate', calculatePaint);

module.exports = router;
