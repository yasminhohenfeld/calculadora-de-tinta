const calculatorSchemas = require('../validations/calculatorSchemas');

const calcularParede = (largura, altura, qtdJanelas, qtdPortas) => {
  const alturaMinimaComPortas = 2.20 + 0.30; 
  if (qtdPortas > 0 && altura < alturaMinimaComPortas) {
    throw new Error(`A altura da parede com portas deve ser no mínimo ${alturaMinimaComPortas} metros.`);
  }

 
  let areaParede = altura * largura;


  const areaJanelas = qtdJanelas * (2.00 * 1.20);
  const areaPortas = qtdPortas * (0.80 * 1.90);
  const areaTotalAberturas = areaJanelas + areaPortas;

  if (areaTotalAberturas > areaParede / 2) {
    throw new Error('A área total de portas e janelas deve ser no máximo 50% da área da parede.');
  }

  areaParede -= areaTotalAberturas;

  if (areaParede < 1 || areaParede > 50) {
    throw new Error('A área da parede deve ser entre 1 e 50 metros quadrados após descontar portas e janelas.');
  }

  return areaParede;
};

const calculatePaint = async (req, res) => {
  try {
    const { paredes } = req.body;
    await calculatorSchemas.validate({ paredes });

    let totalArea = 0;
    let totalOpenings = 0;

    paredes.forEach(wall => {
      try {
        const areaParede = calcularParede(wall.largura, wall.altura, wall.qtdJanelas, wall.qtdPortas);
        const areaJanelas = wall.qtdJanelas * (2.00 * 1.20);
        const areaPortas = wall.qtdPortas * (0.80 * 1.90);
        totalArea += areaParede;
        totalOpenings += (areaJanelas + areaPortas);
      } catch (error) {
        throw new Error(`Erro na parede com largura ${wall.largura}, altura ${wall.altura}, ${error.message}`);
      }
    });

    const areaPintavel = totalArea - totalOpenings;
    if (areaPintavel < 1) {
      throw new Error('Área pintável não pode ser menor que 1 metro quadrado.');
    }

    const latasDeTinta = calcularTinta(areaPintavel);

    res.status(200).json({
      totalArea,
      totalOpenings,
      paintableArea: areaPintavel,
      paintRequired: areaPintavel / 5,
      cans: latasDeTinta,
    });
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

function calcularTinta(areaPintavel) {
  const latasDisponiveis = [18, 3.6, 2.5, 0.5];
  let restante = areaPintavel / 5; 

  const latasDeTinta = latasDisponiveis.reduce((acc, lata) => {
    const qtd = Math.floor(restante / lata);
    if (qtd > 0) {
      acc.push({ tamanho: lata, quantidade: qtd });
      restante -= qtd * lata;
    }
    return acc;
  }, []);

  if (restante > 0) {
    latasDeTinta.push({ tamanho: 0.5, quantidade: Math.ceil(restante / 0.5) });
  }

  return latasDeTinta;
}

module.exports = {
  calculatePaint,
};
