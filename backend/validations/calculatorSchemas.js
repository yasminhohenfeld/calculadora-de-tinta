const yup = require('yup');

const calculatorSchemas = yup.object().shape({
  paredes: yup.array().of(
    yup.object().shape({
      largura: yup.number().min(1).max(50).required(),
      altura: yup.number().min(1).max(50).required(),
      qtdJanelas: yup.number().min(0).required(),
      qtdPortas: yup.number().min(0).required(),
    })
  ).required(),
});

module.exports = calculatorSchemas;