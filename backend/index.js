const express = require('express');
const cors = require('cors');
const router = require('./route/routeCalculator');
const app = express();
const PORT = 3001;

app.use(cors());
app.use(express.json());

app.use('/api', router);

app.listen(PORT, () => {
  console.log(`Servidor rodando na porta ${PORT}`);
});